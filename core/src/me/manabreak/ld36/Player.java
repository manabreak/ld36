package me.manabreak.ld36;

import com.artemis.ComponentMapper;
import com.artemis.World;
import com.artemis.annotations.Wire;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;

import me.manabreak.ld36.components.ActorComponent;
import me.manabreak.ld36.components.PhysicsBody;
import me.manabreak.ld36.components.Tag;
import me.manabreak.ld36.factories.NodeManager;
import me.manabreak.ld36.factories.PlayerFactory;
import me.manabreak.ld36.systems.MapSystem;
import me.manabreak.ld36.systems.ParticleSystem;
import me.manabreak.ld36.systems.PhysicsSystem;
import me.manabreak.ld36.systems.StageSystem;

import static me.manabreak.ld36.systems.PhysicsSystem.INV_SCALE;
import static me.manabreak.ld36.systems.PhysicsSystem.SCALE;

@Wire
public class Player implements MapSystem.Listener, CollisionHandler {

    public static final int S_W = 16;
    public static final int S_H = 24;

    private static final float MOVE_FORCE = 2.1f;
    private static final float MOVE_DECAY = 0.35f;
    private static final float MAX_JUMP_FORCE = 4.7f;
    private static final float MAX_VEL_Y = 20f;
    private static final float RESET_DURATION = 1.3f;
    private static final float ANIM_SPEED = 0.08f;
    private int id;
    private PhysicsBody physicsBody;
    private ActorComponent actorComponent;
    private SpriteActor actor;
    private World world;
    private StageSystem stageSystem;
    private PhysicsSystem physicsSystem;
    private ComponentMapper<PhysicsBody> physicsBodies;
    private ComponentMapper<ActorComponent> actors;
    private ComponentMapper<Tag> tags;
    private Fixture groundSensor;
    private Vector2 input = new Vector2();
    private boolean facingRight = true;
    private boolean moving = false;
    private boolean canJump = false;
    private boolean jumping = false;
    private boolean colliding = false;
    private Body body;
    @SuppressWarnings("unused")
    private MapSystem mapSystem;
    private float lastSpawnX, lastSpawnY;
    private boolean controlsEnabled = false;
    private boolean playerResetting = false;
    private float resetTimer = 0f;
    private TextureRegion region;
    private Sprite sprite;
    private int currentFrame = 0;
    private float animTimer = 0f;
    private int cogs = 0;
    private NodeManager nodeManager;
    private ParticleSystem particleSystem;

    private float lastGroundY;

    public Player(World world) {
        this.world = world;
    }

    public void initialize() {
        id = world.getSystem(PlayerFactory.class).createPlayer();

        region = Res.findRegion("player");

        actorComponent = actors.get(id);
        actorComponent.setSprite(Res.createSprite("player"));
        sprite = actorComponent.getSprite();
        sprite.setSize(8f, 12f);
        setFrame(2, 1);

        actor = actorComponent.getActor();
        actor.offsetX = -4f;
        actor.offsetY = -4f;

        Tag t = tags.get(id);
        t.setTag("PLAYER");

        physicsBody = physicsBodies.get(id);
        physicsBody.body = physicsSystem.createBody(BodyDef.BodyType.DynamicBody, 5f, 8f);
        physicsBody.body.setUserData(physicsBody);
        physicsBody.body.getFixtureList().get(0).setFriction(0f);
        physicsBody.body.getFixtureList().get(0).setRestitution(0f);

        body = physicsBody.body;
        physicsBody.id = id;
        physicsBody.tag = "PLAYER";
        physicsBody.category = PhysicsBody.MASK_PLAYER;

        System.out.println("Player ID: " + id);
        physicsBody = physicsBodies.get(id);
        physicsBody.handler = this;
    }

    private void setFrame(int x, int y) {
        sprite.setRegion(region, x * S_W, y * S_H, S_W, S_H);
    }

    public void act(float dt) {
        if (playerResetting) {
            handleResetting(dt);
        }

        input.set(0f, 0f);
        Body b = physicsBody.body;
        Vector2 v = b.getLinearVelocity();

        if (controlsEnabled) {
            handleInput();
        }

        handleMovement(b, v);
        tryJump(b);
        updateAnimation(dt);

        /*
        if (!jumping && canJump) {
            body.setTransform(body.getPosition().x, lastGroundY + 4f * PhysicsSystem.INV_SCALE, 0f);
        }
        */

        actor.setPosition(b.getPosition().x * SCALE, b.getPosition().y * SCALE);

        lerpCamera(dt);
    }

    private void lerpCamera(float dt) {
        if (!playerResetting) {
            Vector3 camPos = stageSystem.getCamera().position;
            float x = camPos.x + (actor.getX() - camPos.x) * dt * 12f;
            float y = camPos.y + (actor.getY() - camPos.y) * dt * 12f;
            stageSystem.getCamera().position.set(x, y, 0f);
        }
    }

    private void updateAnimation(float dt) {
        if (jumping) {
            setFrame(2, 1);
        } else if (moving) {
            animTimer += dt;
            if (animTimer >= ANIM_SPEED) {
                currentFrame++;
                animTimer -= ANIM_SPEED;
                currentFrame %= 8;
                setFrame(currentFrame, 0);
            }
        } else {
            setFrame(0, 0);
            currentFrame = 0;
            animTimer = 0f;
        }
        sprite.setFlip(!facingRight, false);
    }

    private void tryJump(Body b) {
        if (controlsEnabled && canJump && Gdx.input.isKeyJustPressed(Input.Keys.SPACE)) {
            b.setGravityScale(1f);
            b.applyLinearImpulse(0f, MAX_JUMP_FORCE, 0f, 0f, true);
            currentFrame = 0;
            jumping = true;
            canJump = false;
            Res.jump.play();
        }
    }

    private void handleMovement(Body b, Vector2 v) {
        if (input.x != 0f) {
            v.x = input.x;
            b.setLinearVelocity(v);
            if (v.x != 0f) {
                facingRight = v.x > 0f;
            }

            if (!moving) {
                currentFrame = 1;
                if (v.x < 0f) sprite.setFlip(true, false);
                else sprite.setFlip(false, false);
            }
            moving = true;
        } else {
            v.x *= MOVE_DECAY;
            b.setLinearVelocity(v);
            moving = false;
            sprite.setFlip(!facingRight, false);
        }
    }

    private void handleResetting(float dt) {
        resetTimer += dt;
        if (resetTimer >= RESET_DURATION) {
            body.setTransform(lastSpawnX * INV_SCALE, lastSpawnY * INV_SCALE, 0f);
            body.setLinearVelocity(0f, 0f);
            playerResetting = false;
            controlsEnabled = true;
            actor.setDrawEnabled(true);
        }
    }

    private void handleInput() {
        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            input.x = -MOVE_FORCE;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            input.x = MOVE_FORCE;
        }
    }

    @Override
    public void onBeginCollision(Contact contact, PhysicsBody other) {
        switch (other.category) {
            case PhysicsBody.MASK_WORLD:
            case PhysicsBody.MASK_ELEVATOR:
                handleWallContact(other.category, contact);
                break;
            case PhysicsBody.MASK_COLLECTABLE:
                if (other.id == id) return;
                System.out.println("Collecting cog!");
                nodeManager.cogCollected(other.id);
                cogs++;
                break;
            case PhysicsBody.MASK_SLOT:
                System.out.println("Slot");
                if (cogs > 0) {
                    System.out.println("Placing cog into slot");
                    if (nodeManager.fillSlot(other.body)) {
                        cogs--;
                    }
                } else {
                    System.out.println("No cog to place");
                }
                break;
            case PhysicsBody.MASK_SPIKE:
                System.out.println("Hit spikes");
                resetPlayer();
                particleSystem.spawnBlood(actor.getX(), actor.getY());
                actor.setDrawEnabled(false);
                Res.death.play();
                break;
            case PhysicsBody.MASK_TRIGGER:
                System.out.println("Hit a trigger");
                nodeManager.triggerHit(other.id);
                break;
        }
    }

    private void resetPlayer() {
        controlsEnabled = false;
        playerResetting = true;
        resetTimer = 0f;

    }

    private void handleWallContact(int category, Contact contact) {
        boolean hitGround = false;
        int numContacts = contact.getWorldManifold().getNumberOfContactPoints();
        if (numContacts > 0) {
            Vector2[] points = contact.getWorldManifold().getPoints();
            Vector2 mid = points[0];
            if (numContacts > 1) {
                mid.x = (mid.x + points[1].x) / 2f;
                mid.y = (mid.y + points[1].y) / 2f;
            }

            if (mid.x > body.getPosition().x - 3f * INV_SCALE && mid.x < body.getPosition().x + 3f * INV_SCALE &&
                    mid.y < body.getPosition().y) {
                hitGround = true;
                lastGroundY = mid.y;

                if (category == PhysicsBody.MASK_WORLD) {
                    Res.click.play();
                    particleSystem.spawnDust(actor.getX(), actor.getY() - 6f);
                }
            }
        }

        if (hitGround) {
            canJump = true;
            jumping = false;
        }

        colliding = true;
    }

    @Override
    public void onEndCollision(Contact contact, PhysicsBody other) {

    }

    @Override
    public void onMapLoaded(MapSystem mapSystem) {
        MapObject spawn = mapSystem.getPlayerSpawn();
        if (spawn != null) {
            float x = (float) spawn.getProperties().get("x");
            float y = (float) spawn.getProperties().get("y");
            physicsBody.body.setTransform(x * INV_SCALE, y * INV_SCALE, 0f);
            actor.setPosition(x, y);

            lastSpawnX = x;
            lastSpawnY = y;
        } else {
            lastSpawnX = actor.getX();
            lastSpawnY = actor.getY();
        }
    }

    public void disableControls() {
        controlsEnabled = false;
    }

    public void enableControls() {
        controlsEnabled = true;
    }
}
