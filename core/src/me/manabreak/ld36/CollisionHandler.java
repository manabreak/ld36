package me.manabreak.ld36;

import com.badlogic.gdx.physics.box2d.Contact;

import me.manabreak.ld36.components.PhysicsBody;

public interface CollisionHandler {
    void onBeginCollision(Contact contact, PhysicsBody other);

    void onEndCollision(Contact contact, PhysicsBody other);
}
