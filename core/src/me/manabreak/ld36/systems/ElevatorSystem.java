package me.manabreak.ld36.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.systems.IteratingSystem;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.Body;

import me.manabreak.ld36.components.Elevator;
import me.manabreak.ld36.components.PhysicsBody;

public class ElevatorSystem extends IteratingSystem {

    private ComponentMapper<PhysicsBody> physicsBodies;
    private ComponentMapper<Elevator> elevators;

    public ElevatorSystem() {
        super(Aspect.all(Elevator.class, PhysicsBody.class));
    }

    @Override
    protected void inserted(int id) {
        Elevator e = elevators.get(id);
        Body b = physicsBodies.get(id).body;
        e.startX = b.getPosition().x * PhysicsSystem.SCALE;
        e.startY = b.getPosition().y * PhysicsSystem.SCALE;
        if (e.inverted) {
            e.startY += e.distance;
            e.timer = MathUtils.PI;
        }
    }

    @Override
    protected void process(int id) {
        Elevator e = elevators.get(id);
        Body b = physicsBodies.get(id).body;
        e.timer += world.delta;
        b.setTransform(b.getPosition().x, b.getPosition().y + e.distance * PhysicsSystem.INV_SCALE * MathUtils.sin(e.timer), 0f);
    }
}
