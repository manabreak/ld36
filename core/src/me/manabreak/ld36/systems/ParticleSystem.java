package me.manabreak.ld36.systems;

import com.artemis.BaseSystem;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Stage;

import java.util.ArrayList;
import java.util.List;

import me.manabreak.ld36.Res;
import me.manabreak.ld36.SpriteActor;

public class ParticleSystem extends BaseSystem {

    private StageSystem stageSystem;
    private List<BloodParticle> bloodPool = new ArrayList<>();
    private List<DustParticle> dustPool = new ArrayList<>();
    private int nextDust = 0;

    @Override
    protected void initialize() {
        for (int i = 0; i < 30; ++i) {
            bloodPool.add(new BloodParticle(Res.createSprite("white")));
        }
        for (int i = 0; i < 100; ++i) {
            dustPool.add(new DustParticle(Res.createSprite("white")));
        }
    }

    @Override
    protected void processSystem() {

    }

    public void spawnBlood(float x, float y) {
        for (BloodParticle sa : bloodPool) {
            sa.spawn(stageSystem.getStage(), x, y);
        }
    }

    public void spawnDust(float x, float y) {
        for (int i = nextDust; i < nextDust + 25; ++i) {
            DustParticle d = dustPool.get(i);
            d.spawn(stageSystem.getStage(), x, y);
        }
        nextDust += 25;
        nextDust %= dustPool.size();
    }

    private static class DustParticle extends SpriteActor {
        float dx, dy;
        float life = 0f;

        public DustParticle(Sprite sprite) {
            super(sprite);
            setColor(0.7f, 0.6f, 0.65f, 0.1f);
            float size = MathUtils.random(0.35f, 0.7f);
            sprite.setSize(size, size);
        }

        public void spawn(Stage stage, float x, float y) {
            setPosition(x, y);
            dx = MathUtils.random(-1f, 1f);
            dy = MathUtils.random(0.3f, 0.8f);
            life = 0f;
            stage.addActor(this);
        }

        @Override
        public void act(float dt) {
            life += dt;
            dy *= 0.85f;
            dx *= 0.8f;
            moveBy(dx, dy);
            sprite.setAlpha(0.3f * (1f - life / 2f));
            if (life >= 2f) {
                remove();
            }
        }
    }

    private static class BloodParticle extends SpriteActor {
        float dx, dy;
        float life = 0f;

        public BloodParticle(Sprite sprite) {
            super(sprite);
            setColor(1f, 0f, 0f, 1f);
            sprite.setSize(MathUtils.random(1f, 2f), MathUtils.random(1f, 2f));
        }

        void spawn(Stage stage, float x, float y) {
            setPosition(x, y);
            dx = MathUtils.random(-1f, 1f);
            dy = MathUtils.random(1f, 2f);
            life = 0f;
            stage.addActor(this);
        }

        @Override
        public void act(float dt) {
            life += dt;
            dy -= 2f * life * life;
            dx *= 0.98f;
            moveBy(dx, dy);
            sprite.setAlpha(1f - life / 2f);
            if (life >= 2f) {
                remove();
            }
        }
    }
}
