package me.manabreak.ld36.systems;

import com.artemis.BaseSystem;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import java.util.ArrayList;
import java.util.List;

import me.manabreak.ld36.Res;
import me.manabreak.ld36.SpriteActor;

public class GuiSystem extends BaseSystem {

    private final SpriteActor fadeActor;
    private Stage stage;
    private Label dialogLabelOne;
    private Label dialogLabelTwo;
    private Label currentLabel;

    private List<Dust> dustActors = new ArrayList<>();

    public GuiSystem() {
        stage = new Stage(new ScreenViewport());

        fadeActor = new SpriteActor(Res.createSprite("fade"));
        fadeActor.getSprite().setSize(stage.getWidth(), stage.getHeight());
        stage.addActor(fadeActor);

        Label.LabelStyle dialogLabelStyle = new Label.LabelStyle(Res.font, Color.WHITE);
        dialogLabelOne = new Label("", dialogLabelStyle);
        dialogLabelTwo = new Label("", dialogLabelStyle);
        currentLabel = dialogLabelOne;

        for (int i = 0; i < 40; ++i) {
            SpriteActor a = new SpriteActor(Res.createSprite("white"));
            a.getSprite().setColor(1f, 0.8f, 0.4f, 0.15f);
            a.setPosition(MathUtils.random(stage.getWidth()), MathUtils.random(stage.getHeight()));
            dustActors.add(new Dust(a));
            a.setSize(4f, 4f);
            stage.addActor(a);
        }
    }

    private void swapLabels() {
        if (currentLabel == dialogLabelOne) {
            currentLabel = dialogLabelTwo;
        } else {
            currentLabel = dialogLabelOne;
        }
    }

    @Override
    protected void processSystem() {
        for (Dust d : dustActors) {
            d.act(world.delta);
        }

        stage.act(world.delta);
        stage.draw();
    }

    public void showDialog(String text) {
        currentLabel.setText(text);
        currentLabel.pack();
        currentLabel.setPosition(stage.getWidth() / 2f - currentLabel.getWidth() / 2f, 56f + stage.getHeight() / 2f - currentLabel.getHeight() / 2f);
        stage.addActor(currentLabel);

        currentLabel.clearActions();
        currentLabel.addAction(Actions.sequence(
                Actions.alpha(0f),
                Actions.fadeIn(0.4f, Interpolation.fade)
        ));
    }

    public void hideDialog() {
        currentLabel.clearActions();
        currentLabel.addAction(Actions.sequence(
                Actions.fadeOut(0.4f, Interpolation.fade),
                Actions.removeActor()
        ));
        swapLabels();
    }

    private static class Dust {
        SpriteActor a;
        Vector2 d = new Vector2(0f, 1f);

        public Dust(SpriteActor a) {
            this.a = a;
        }

        public void act(float dt) {
            d.rotate(MathUtils.random(-5f, 5f));
            a.moveBy(d.x, d.y);

            if (a.getX() < 0f) a.moveBy(a.getStage().getWidth(), 0f);
            if (a.getX() > a.getStage().getWidth()) a.moveBy(-a.getStage().getWidth(), 0f);
            if (a.getY() < 0f) a.moveBy(0f, a.getStage().getHeight());
            if (a.getY() > a.getStage().getHeight()) a.moveBy(0f, -a.getStage().getHeight());
        }
    }
}
