package me.manabreak.ld36.systems;

import com.artemis.BaseSystem;
import com.artemis.annotations.Wire;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;

@Wire
public class PhysicsRenderingSystem extends BaseSystem {

    private PhysicsSystem physicsSystem;
    private StageSystem stageSystem;
    private Box2DDebugRenderer debugRenderer;

    public PhysicsRenderingSystem() {
        debugRenderer = new Box2DDebugRenderer();
    }

    @Override
    protected void processSystem() {
        Matrix4 m = stageSystem.getCamera().combined.cpy();
        m.scl(PhysicsSystem.SCALE);
        debugRenderer.render(physicsSystem.getPhysicsWorld(), m);
    }
}
