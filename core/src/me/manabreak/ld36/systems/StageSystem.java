package me.manabreak.ld36.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.annotations.Wire;
import com.artemis.systems.IteratingSystem;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.Viewport;

import me.manabreak.ld36.components.ActorComponent;

/**
 * A simple system that wraps a stage and handles
 * insertion and removal of actors.
 */
@Wire
public class StageSystem extends IteratingSystem {

    private final Stage stage;
    private ComponentMapper<ActorComponent> actors;
    private float shakeTimer = 0f;
    private float shakeDuration = 1f;
    private float shakeForce = 1f;
    private boolean shaking = false;

    /**
     * Constructs a new stage system with the given viewport.
     *
     * @param viewport Viewport to use
     */
    public StageSystem(Viewport viewport) {
        super(Aspect.all(ActorComponent.class));
        stage = new Stage(viewport);
    }

    @Override
    public void inserted(int e) {
        stage.getRoot().addActorAt(0, actors.get(e).actor);
    }

    @Override
    public void removed(int e) {
        actors.get(e).actor.remove();
    }

    @Override
    protected void process(int entityId) {

    }

    @Override
    protected void end() {
        stage.act(world.getDelta());
        if (shaking) {
            shakeTimer += world.delta;
            if (shakeTimer >= shakeDuration) shaking = false;

            shakeForce = MathUtils.sin((shakeTimer / shakeDuration) * 180f) * 0.5f;

            float x = MathUtils.random(-shakeForce, shakeForce);
            float y = MathUtils.random(-shakeDuration, shakeForce);
            getCamera().position.add(x, y, 0f);
        }
        stage.draw();
    }

    public Stage getStage() {
        return stage;
    }

    public OrthographicCamera getCamera() {
        return (OrthographicCamera) stage.getCamera();
    }

    public void shakeCamera(float time) {
        shakeTimer = 0f;
        shakeForce = 1f;
        shakeDuration = time;
        shaking = true;
    }
}
