package me.manabreak.ld36.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.systems.IteratingSystem;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

import java.util.ArrayList;
import java.util.List;

import me.manabreak.ld36.SpriteActor;
import me.manabreak.ld36.components.ActorComponent;
import me.manabreak.ld36.components.PhysicsBody;

public class PhysicsSystem extends IteratingSystem implements ContactListener, MapSystem.Listener {

    public static final float SCALE = 24f;
    public static final float INV_SCALE = 1f / SCALE;

    private final World world;
    int count = 0;
    private ComponentMapper<ActorComponent> actors;
    private ComponentMapper<PhysicsBody> physicsBodies;
    private PolygonShape polygonShape;
    private StageSystem stageSystem;
    private List<Body> wallBodies = new ArrayList<>();
    private boolean debugDraw = false;

    public PhysicsSystem() {
        super(Aspect.all(PhysicsBody.class));
        world = new World(new Vector2(0f, -15f), false);
        world.setContactListener(this);
        polygonShape = new PolygonShape();
    }

    @Override
    protected void dispose() {
        polygonShape.dispose();
        super.dispose();
    }

    @Override
    protected void begin() {
        world.step(getWorld().getDelta(), 6, 2);
    }

    public World getPhysicsWorld() {
        return world;
    }

    @Override
    protected void inserted(int id) {
        count++;
    }

    @Override
    protected void removed(int id) {
        count--;
        PhysicsBody physicsBody = physicsBodies.get(id);
        if (physicsBody != null && physicsBody.body != null) {
            world.destroyBody(physicsBody.body);
        }
    }

    @Override
    protected void process(int id) {
        if (actors.has(id)) {
            SpriteActor a = actors.get(id).getActor();
            Body body = physicsBodies.get(id).body;
            a.setPosition(body.getPosition().x * SCALE, body.getPosition().y * SCALE);
        }
    }

    private Body createBody(BodyDef.BodyType type) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = type;
        return world.createBody(bodyDef);
    }

    public Body createBody(BodyDef.BodyType type, float width, float height) {
        Body body = createBody(type);
        createBoxFixture(width, height, body, false);
        return body;
    }

    private void createBoxFixture(float width, float height, Body body, boolean sensor) {
        polygonShape.setAsBox((width * INV_SCALE) / 2f, (height * INV_SCALE) / 2f);
        FixtureDef f = new FixtureDef();
        f.shape = polygonShape;
        f.isSensor = sensor;
        body.createFixture(f);
    }

    public Body createWall(float width, float height) {
        Body body = createBody(BodyDef.BodyType.StaticBody);
        createBoxFixture(width, height, body, false);
        return body;
    }

    public Body createSensor(float width, float height) {
        Body body = createBody(BodyDef.BodyType.StaticBody);
        createBoxFixture(width, height, body, true);
        return body;
    }

    @Override
    public void beginContact(Contact contact) {
        Body bodyA = contact.getFixtureA().getBody();
        Body bodyB = contact.getFixtureB().getBody();

        PhysicsBody p0 = (PhysicsBody) bodyA.getUserData();
        PhysicsBody p1 = (PhysicsBody) bodyB.getUserData();

        if (p0 != null && p0.handler != null) {
            if (p0.tag.equals("PLAYER") && p1.tag.equals("COG")) {
                System.out.println("0: " + p0.tag + " --> " + p1.tag);
            }
            p0.handler.onBeginCollision(contact, p1);
        }

        if (p1 != null && p1.handler != null) {
            System.out.println("1: " + p1.tag + " --> " + p0.tag);
            p1.handler.onBeginCollision(contact, p0);
        }

    }

    @Override
    public void endContact(Contact contact) {
        Body bodyA = contact.getFixtureA().getBody();
        Body bodyB = contact.getFixtureB().getBody();

        PhysicsBody p0 = (PhysicsBody) bodyA.getUserData();
        PhysicsBody p1 = (PhysicsBody) bodyB.getUserData();

        if (p0 != null && p0.handler != null) {
            p0.handler.onEndCollision(contact, p1);
        }
        if (p1 != null && p1.handler != null) {
            p1.handler.onEndCollision(contact, p0);
        }
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }

    @Override
    public void onMapLoaded(MapSystem mapSystem) {
        clearWalls();
        MapLayer layer = mapSystem.getCurrentMap().getLayers().get("P");
        int wallCount = 0;
        for (MapObject obj : layer.getObjects()) {
            if ((int) obj.getProperties().get("id") == 102) {
                System.out.println("Creating wall 102");
            }
            float x = (float) obj.getProperties().get("x");
            float y = (float) obj.getProperties().get("y");
            float w = (float) obj.getProperties().get("width");
            float h = (float) obj.getProperties().get("height");
            Body b = createWall(w, h);
            PhysicsBody physicsBody = new PhysicsBody();
            physicsBody.category = PhysicsBody.MASK_WORLD;
            physicsBody.tag = "WALL";
            b.setUserData(physicsBody);
            float bx = (x + w / 2f) * INV_SCALE;
            float by = (y + h / 2f) * INV_SCALE;
            b.setTransform(bx, by, 0f);
            wallBodies.add(b);
            wallCount++;
        }
        System.out.println("Created walls, wallcount: " + wallCount);
    }

    private void clearWalls() {
        System.out.println("Destroying " + wallBodies.size() + " walls.");
        for (Body body : wallBodies) {
            world.destroyBody(body);
        }
        wallBodies.clear();
    }


}
