package me.manabreak.ld36.systems;

import com.artemis.BaseSystem;
import com.artemis.annotations.Wire;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.MathUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MapSystem extends BaseSystem {

    private final List<Listener> listeners = new ArrayList<>();
    @Wire
    private StageSystem stageSystem;
    private TiledMap currentMap;
    private TiledMap prevMap = null;
    private OrthogonalTiledMapRenderer renderer;
    private OrthogonalTiledMapRenderer prevRenderer = null;
    private MapObject playerSpawn;

    private int level = 0;
    private float levelChangeTimer = 0f;
    private boolean levelFading = false;
    private boolean mapChangeQueued = false;
    private DirectorSystem directorSystem;

    @Override
    protected void initialize() {
        setEnabled(false);
    }

    public void nextLevel() {
        mapChangeQueued = true;
    }

    @Override
    protected void begin() {
        if (mapChangeQueued) {
            mapChangeQueued = false;
            loadMap(++level);
        }
    }

    public void loadMap(final int level) {
        this.level = level;
        if (level == 6) {
            System.out.println("Final level completed!");
            directorSystem.doEndingDialog();
            return;
        }

        prevMap = currentMap;
        prevRenderer = renderer;
        if (prevMap != null) {
            levelFading = true;
            levelChangeTimer = 0f;
            stageSystem.shakeCamera(2f);
        }
        currentMap = new TmxMapLoader().load(String.format("maps/%s.tmx", String.format(Locale.ENGLISH, "%03d", level)));
        renderer = new OrthogonalTiledMapRenderer(currentMap);
        if (levelFading) {
            renderer.getBatch().setColor(1f, 1f, 1f, 0f);
        }
        for (Listener l : listeners) {
            l.onMapLoaded(this);
        }
        setEnabled(true);
    }

    @Override
    protected void processSystem() {

        if (levelFading) {
            levelChangeTimer += world.delta;
            if (levelChangeTimer >= 3f) {
                prevRenderer = null;
                levelFading = false;
                prevMap = null;
            } else {
                float mapAlpha = levelChangeTimer / 3f;
                renderer.getBatch().setColor(1f, 1f, 1f, mapAlpha);
                prevRenderer.getBatch().setColor(1f, 1f, 1f, MathUtils.clamp(1f - mapAlpha, 0f, 1f));
                prevRenderer.setView(stageSystem.getCamera());
                prevRenderer.render();
            }
        }

        renderer.setView(stageSystem.getCamera());
        renderer.render();
    }

    public void addListener(Listener listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    public void removeListener(Listener listener) {
        listeners.remove(listener);
    }

    public TiledMap getCurrentMap() {
        return currentMap;
    }

    public MapObject getPlayerSpawn() {
        return getNode("Player");
    }

    private MapObject getNode(String name) {
        if (currentMap != null) {
            return currentMap.getLayers().get("N").getObjects().get(name);
        } else {
            System.err.println("Current map is null while trying to get spawn for " + name);
        }
        return null;
    }

    public int getLevel() {
        return level;
    }


    public interface Listener {
        void onMapLoaded(MapSystem mapSystem);
    }
}
