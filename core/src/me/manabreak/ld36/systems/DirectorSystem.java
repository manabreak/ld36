package me.manabreak.ld36.systems;

import com.artemis.BaseSystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import me.manabreak.ld36.Player;

public class DirectorSystem extends BaseSystem implements MapSystem.Listener {

    private Player player;
    private float dialogTimer = 0f;
    private Queue<Dialog> dialogs = new LinkedList<>();
    private Dialog currentDialog = null;
    private GuiSystem gui;
    private StageSystem stageSystem;

    public DirectorSystem() {

    }

    @Override
    protected void initialize() {
        setEnabled(false);
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    @Override
    protected void processSystem() {
        if (currentDialog != null) {
            if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) {
                currentDialog = dialogs.poll();
                if (currentDialog != null) {
                    currentDialog.start();
                }

            } else if (currentDialog.act(world.delta)) {
                currentDialog = dialogs.poll();
                if (currentDialog != null) {
                    currentDialog.start();
                }
            }
        }
    }

    @Override
    public void onMapLoaded(MapSystem mapSystem) {
        System.out.println("mapSystem = [" + mapSystem.getLevel() + "]");
        if (mapSystem.getLevel() == 0) {
            dialogs.add(new DisableControls(player));
            dialogs.add(new TextDialog(gui, "Phew!"));
            dialogs.add(new PauseDialog(0.2f));
            dialogs.add(new TextDialog(gui, "Made it inside the tomb in one piece..."));
            dialogs.add(new PauseDialog(1f));
            dialogs.add(new TextDialog(gui, "... I think."));
            dialogs.add(new TextDialog(gui, "Well, time to raid this tomb."));
            dialogs.add(new TextDialog(gui, "Hopefully there's no funny stuff going around..."));
            dialogs.add(new EnableControls(player));
            startDialog();

        } else if (mapSystem.getLevel() == 1) {
            currentDialog = null;
            dialogs.clear();
            dialogs.add(new DisableControls(player));
            dialogs.add(new TextDialog(gui, "What the..?"));
            dialogs.add(new TextDialog(gui, "The tomb's shaking!"));
            dialogs.add(new TextDialog(gui, "So there IS funny stuff! I knew it!"));
            dialogs.add(new EnableControls(player));
            startDialog();
        }
    }

    public void doDialog(List<String> texts) {
        for (String s : texts) {
            dialogs.add(new TextDialog(gui, s));
        }
        startDialog();
    }

    private void startDialog() {
        currentDialog = dialogs.poll();
        currentDialog.start();
        setEnabled(true);
    }

    public void doEndingDialog() {
        currentDialog = null;
        dialogs.clear();
        dialogs.add(new DisableControls(player));
        dialogs.add(new TextDialog(gui, "..?"));
        dialogs.add(new TextDialog(gui, "That's it?"));
        dialogs.add(new TextDialog(gui, "..."));
        dialogs.add(new TextDialog(gui, "Well, time to climb back up."));
        dialogs.add(new TextDialog(gui, "Maybe the NEXT tomb has at least SOME loot."));
        dialogs.add(new TextDialog(gui, "In the meantime..."));
        dialogs.add(new TextDialog(gui, "Thanks for playing! Hope you had a good time!"));
        dialogs.add(new EnableControls(player));
        startDialog();
    }


    public static abstract class Dialog {
        protected float timer = 0f;

        private boolean started = false;

        public void start() {
            if (started) return;
            started = true;
            onStart();
        }

        protected void onStart() {

        }

        public abstract boolean act(float dt);

        public void clear() {

        }
    }

    public static class TextDialog extends Dialog {

        private final GuiSystem gui;
        private final String text;

        public TextDialog(GuiSystem gui, String text) {
            this.gui = gui;
            this.text = text;
        }

        @Override
        protected void onStart() {
            System.out.println("> " + text);
            gui.showDialog(text);
        }

        @Override
        public void clear() {
            gui.hideDialog();
        }

        @Override
        public boolean act(float dt) {
            timer += dt;
            boolean done = timer >= 3f;
            if (done) {
                gui.hideDialog();
            }
            return done;
        }
    }

    public static class PauseDialog extends Dialog {
        private final float time;

        public PauseDialog(float time) {
            this.time = time;
        }

        @Override
        protected void onStart() {
            System.out.println("(Waiting)");
        }

        @Override
        public boolean act(float dt) {
            timer += dt;
            return timer >= time;
        }
    }

    private static class DisableControls extends Dialog {
        private final Player player;

        private DisableControls(Player player) {
            this.player = player;
        }

        @Override
        protected void onStart() {
            System.out.println("(Disable controls)");
        }

        @Override
        public boolean act(float dt) {
            player.disableControls();
            return true;
        }
    }

    private static class EnableControls extends Dialog {

        private final Player player;

        private EnableControls(Player player) {
            this.player = player;
        }

        @Override
        protected void onStart() {
            System.out.println("(Enable controls)");
        }

        @Override
        public boolean act(float dt) {
            player.enableControls();
            return true;
        }
    }
}
