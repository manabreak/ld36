package me.manabreak.ld36;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;

public class Res {

    public static final String ATLAS_PATH = "graphics/game.atlas";
    public static BitmapFont font;
    public static Music song1Intro;
    public static Music song1Loop;
    public static Sound jump, death, collection, placement, rumble, click;
    private static TextureAtlas atlas;

    public static Sprite createSprite(String name) {
        return atlas.createSprite(name);
    }

    public static TextureRegion findRegion(String name) {
        return atlas.findRegion(name);
    }

    public static void load() {
        if (!Gdx.files.internal(ATLAS_PATH).exists()) {
            atlas = new TextureAtlas();
        } else {
            atlas = new TextureAtlas(ATLAS_PATH);
        }

        song1Intro = Gdx.audio.newMusic(Gdx.files.internal("music/song1_intro.ogg"));
        song1Loop = Gdx.audio.newMusic(Gdx.files.internal("music/song1_loop.ogg"));

        jump = Gdx.audio.newSound(Gdx.files.internal("sfx/jump.ogg"));
        death = Gdx.audio.newSound(Gdx.files.internal("sfx/death.ogg"));
        collection = Gdx.audio.newSound(Gdx.files.internal("sfx/collection.ogg"));
        placement = Gdx.audio.newSound(Gdx.files.internal("sfx/placement.ogg"));
        rumble = Gdx.audio.newSound(Gdx.files.internal("sfx/rumble.ogg"));
        click = Gdx.audio.newSound(Gdx.files.internal("sfx/click.ogg"));

        song1Intro.play();
        song1Intro.setOnCompletionListener(music -> {
            song1Loop.setLooping(true);
            song1Loop.play();
        });

        FreeTypeFontGenerator.FreeTypeFontParameter params = new FreeTypeFontGenerator.FreeTypeFontParameter();
        params.size = 16;
        params.color = Color.WHITE;
        font = new FreeTypeFontGenerator(Gdx.files.internal("kenpixel.ttf")).generateFont(params);
    }
}
