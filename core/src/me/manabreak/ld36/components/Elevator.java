package me.manabreak.ld36.components;

import com.artemis.Component;

public class Elevator extends Component {
    public float timer = 0f;
    public float distance = 0.2f;
    public float startX = 0f;
    public float startY = 0f;
    public boolean inverted = false;
}
