package me.manabreak.ld36;

import com.badlogic.gdx.graphics.g2d.Sprite;

public interface CollisionCallback {
    void onCollision(Sprite first, Sprite other);
}
