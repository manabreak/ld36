package me.manabreak.ld36.factories;

import com.artemis.Archetype;
import com.artemis.ArchetypeBuilder;
import com.artemis.ComponentMapper;
import com.artemis.utils.IntBag;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;

import java.util.ArrayList;
import java.util.List;

import me.manabreak.ld36.Res;
import me.manabreak.ld36.components.ActorComponent;
import me.manabreak.ld36.components.Elevator;
import me.manabreak.ld36.components.PhysicsBody;
import me.manabreak.ld36.systems.DirectorSystem;
import me.manabreak.ld36.systems.MapSystem;
import me.manabreak.ld36.systems.PhysicsSystem;

public class NodeManager extends BaseFactory implements MapSystem.Listener {

    private Archetype cogArchetype;
    private Archetype slotArchetype;
    private Archetype spikeArchetype;
    private Archetype triggerArchetype;
    private Archetype elevatorArchetype;

    private ComponentMapper<PhysicsBody> physicsBodies;
    private ComponentMapper<ActorComponent> actors;
    private PhysicsSystem physicsSystem;
    private List<Slot> slots = new ArrayList<>();
    private List<Node> cogs = new ArrayList<>();
    private List<Node> spikes = new ArrayList<>();
    private List<Trigger> triggers = new ArrayList<>();
    private List<Node> elevators = new ArrayList<>();
    private float r, g, b;
    private MapSystem mapSystem;
    private DirectorSystem directorSystem;
    private IntBag cogsForSlots = new IntBag();
    private ComponentMapper<Elevator> elevatorComps;

    @Override
    protected void createArchetypes() {
        //noinspection unchecked
        cogArchetype = new ArchetypeBuilder()
                .add(
                        PhysicsBody.class,
                        ActorComponent.class
                )
                .build(world);

        //noinspection unchecked
        slotArchetype = new ArchetypeBuilder()
                .add(PhysicsBody.class).build(world);

        spikeArchetype = new ArchetypeBuilder()
                .add(PhysicsBody.class).build(world);

        triggerArchetype = new ArchetypeBuilder()
                .add(PhysicsBody.class).build(world);

        elevatorArchetype = new ArchetypeBuilder()
                .add(
                        PhysicsBody.class,
                        Elevator.class,
                        ActorComponent.class
                ).build(world);
    }

    private void createCog(MapObject spawn) {
        int id = world.create(cogArchetype);
        System.out.println("Cog " + id);

        PhysicsBody physicsBody = physicsBodies.get(id);
        physicsBody.body = physicsSystem.createSensor(8f, 8f);
        physicsBody.body.setUserData(physicsBody);
        physicsBody.category = PhysicsBody.MASK_COLLECTABLE;
        physicsBody.mask = PhysicsBody.MASK_PLAYER;
        physicsBody.tag = "COG";
        physicsBody.id = id;

        placeBodyAtSpawn(physicsBody, spawn);

        ActorComponent ac = actors.get(id);
        ac.setSprite(Res.createSprite("cog"));
        ac.getSprite().setSize(8f, 8f);
        ac.getActor().offsetX = -4f;
        ac.getActor().offsetY = -4f;

        cogs.add(new Node(id, physicsBody.body));
    }

    private void placeBodyAtSpawn(PhysicsBody body, MapObject spawn) {
        float x = (float) spawn.getProperties().get("x") * PhysicsSystem.INV_SCALE;
        float y = (float) spawn.getProperties().get("y") * PhysicsSystem.INV_SCALE;
        float w = (float) spawn.getProperties().get("width") * PhysicsSystem.INV_SCALE;
        float h = (float) spawn.getProperties().get("height") * PhysicsSystem.INV_SCALE;
        body.body.setTransform(x + w / 2f, y + h / 2f, 0f);
    }

    private void createSlot(MapObject spawn) {
        int id = world.create(slotArchetype);
        System.out.println("Slot " + id);

        PhysicsBody physicsBody = physicsBodies.get(id);
        physicsBody.body = physicsSystem.createSensor(8f, 8f);
        physicsBody.body.setUserData(physicsBody);
        physicsBody.category = PhysicsBody.MASK_SLOT;
        physicsBody.mask = PhysicsBody.MASK_PLAYER;
        physicsBody.id = id;

        placeBodyAtSpawn(physicsBody, spawn);

        slots.add(new Slot(id, physicsBody.body));
    }

    private void createSpike(MapObject spawn) {
        int id = world.create(spikeArchetype);
        System.out.println("Spike " + id);

        float w = (float) spawn.getProperties().get("width");
        float h = (float) spawn.getProperties().get("height");

        PhysicsBody physicsBody = physicsBodies.get(id);
        physicsBody.body = physicsSystem.createSensor(w, h);
        physicsBody.body.setUserData(physicsBody);
        physicsBody.category = PhysicsBody.MASK_SPIKE;
        physicsBody.mask = PhysicsBody.MASK_PLAYER;
        physicsBody.id = id;

        placeBodyAtSpawn(physicsBody, spawn);

        spikes.add(new Node(id, physicsBody.body));
    }

    private void createElevator(MapObject spawn) {
        int id = world.create(elevatorArchetype);
        System.out.println("Trigger " + id);

        float w = (float) spawn.getProperties().get("width");
        float h = (float) spawn.getProperties().get("height");

        if (spawn.getProperties().containsKey("inverted")) {
            Elevator e = elevatorComps.get(id);
            e.inverted = Boolean.parseBoolean((String) spawn.getProperties().get("inverted"));
        }

        PhysicsBody physicsBody = physicsBodies.get(id);
        physicsBody.body = physicsSystem.createBody(BodyDef.BodyType.KinematicBody, w * 0.9f, h * 0.9f);
        physicsBody.body.setUserData(physicsBody);
        physicsBody.id = id;
        physicsBody.category = PhysicsBody.MASK_ELEVATOR;

        placeBodyAtSpawn(physicsBody, spawn);

        ActorComponent ac = actors.create(id);
        ac.setSprite(Res.createSprite("elevator"));
        ac.getActor().offsetX = -Res.findRegion("elevator").getRegionWidth() / 2f;
        ac.getActor().offsetY = -Res.findRegion("elevator").getRegionHeight() / 2f;

        elevators.add(new Node(id, physicsBody.body));
    }

    private void createTrigger(MapObject spawn) {
        int id = world.create(spikeArchetype);
        System.out.println("Trigger " + id);

        float w = (float) spawn.getProperties().get("width");
        float h = (float) spawn.getProperties().get("height");

        PhysicsBody physicsBody = physicsBodies.get(id);
        physicsBody.body = physicsSystem.createSensor(w, h);
        physicsBody.body.setUserData(physicsBody);
        physicsBody.category = PhysicsBody.MASK_TRIGGER;
        physicsBody.mask = PhysicsBody.MASK_PLAYER;
        physicsBody.id = id;

        placeBodyAtSpawn(physicsBody, spawn);

        Trigger t = null;
        String type = (String) spawn.getProperties().get("type");
        if (type.equalsIgnoreCase("text")) {
            List<String> texts = new ArrayList<>();
            texts.add((String) spawn.getProperties().get("text0"));
            if (spawn.getProperties().containsKey("text1")) {
                texts.add((String) spawn.getProperties().get("text1"));
            }
            if (spawn.getProperties().containsKey("text2")) {
                texts.add((String) spawn.getProperties().get("text2"));
            }
            t = new TextTrigger(id, physicsBody.body, directorSystem, texts);
        }

        if (t != null) {
            triggers.add(t);
        }
    }

    @Override
    public void onMapLoaded(MapSystem mapSystem) {

        for (Node n : cogs) {
            world.delete(n.id);
        }
        cogs.clear();

        for (int i = 0; i < cogsForSlots.size(); ++i) {
            world.delete(cogsForSlots.get(i));
        }
        cogsForSlots.clear();

        for (Slot s : slots) {
            world.delete(s.id);
        }
        slots.clear();

        for (Node n : spikes) {
            world.delete(n.id);
        }
        spikes.clear();

        for (Trigger trigger : triggers) {
            world.delete(trigger.id);
        }
        triggers.clear();

        for (Node n : elevators) {
            world.delete(n.id);
        }

        for (MapObject obj : mapSystem.getCurrentMap().getLayers().get("N").getObjects()) {
            if (obj.getName() != null) {
                if (obj.getName().equals("Spike")) {
                    createSpike(obj);
                } else if (obj.getName().equals("Cog")) {
                    createCog(obj);
                } else if (obj.getName().equals("Slot")) {
                    createSlot(obj);
                } else if (obj.getName().equals("Trigger")) {
                    createTrigger(obj);
                } else if (obj.getName().equalsIgnoreCase("Elevator")) {
                    createElevator(obj);
                }
            }
        }

        System.out.println("Cogs: " + cogs.size());
        System.out.println("Slots: " + slots.size());
    }


    public boolean isLevelComplete() {
        for (Slot s : slots) {
            if (s.empty) return false;
        }
        return true;
    }

    public boolean fillSlot(Body body) {
        for (Slot s : slots) {
            if (s.body == body && s.empty) {
                s.empty = false;
                createCogForSlot(s.body);
                Res.placement.play();
                if (isLevelComplete()) {
                    Res.rumble.play();
                    mapSystem.nextLevel();
                }
                return true;
            }
        }
        return false;
    }

    private void createCogForSlot(Body body) {
        int id = world.create();
        ActorComponent ac = actors.create(id);
        ac.setSprite(Res.createSprite("cog"));
        ac.getActor().setPosition(
                body.getPosition().x * PhysicsSystem.SCALE - ac.getSprite().getWidth() / 2f,
                body.getPosition().y * PhysicsSystem.SCALE - ac.getSprite().getHeight() / 2f
        );
        cogsForSlots.add(id);
    }

    public void triggerHit(int id) {
        for (Trigger t : triggers) {
            if (t.id == id) {
                t.hit();
                world.delete(id);
                triggers.remove(t);
                break;
            }
        }
    }

    public void cogCollected(int id) {
        world.delete(id);
        for (Node n : cogs) {
            if (n.id == id) {
                cogs.remove(n);
                Res.collection.play();
                break;
            }
        }
    }

    private static class Node {
        final int id;
        final Body body;

        public Node(int id, Body body) {
            this.id = id;
            this.body = body;
        }
    }

    private static class Slot extends Node {
        boolean empty = true;

        public Slot(int id, Body body) {
            super(id, body);
        }
    }

    private static abstract class Trigger extends Node {
        public Trigger(int id, Body body) {
            super(id, body);
        }

        public abstract void hit();
    }

    private static class TextTrigger extends Trigger {
        final DirectorSystem directorSystem;
        final List<String> texts;

        public TextTrigger(int id, Body body, DirectorSystem directorSystem, List<String> texts) {
            super(id, body);
            this.directorSystem = directorSystem;
            this.texts = texts;
        }

        @Override
        public void hit() {
            directorSystem.doDialog(texts);
        }
    }
}
