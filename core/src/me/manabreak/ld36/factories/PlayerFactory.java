package me.manabreak.ld36.factories;

import com.artemis.Archetype;
import com.artemis.ArchetypeBuilder;
import com.artemis.ComponentMapper;
import com.artemis.annotations.Wire;

import me.manabreak.ld36.components.ActorComponent;
import me.manabreak.ld36.components.PhysicsBody;
import me.manabreak.ld36.components.Tag;
import me.manabreak.ld36.systems.PhysicsSystem;

@Wire
public class PlayerFactory extends BaseFactory {

    private Archetype playerArchetype;

    private PhysicsSystem physicsSystem;
    private ComponentMapper<ActorComponent> actors;
    private ComponentMapper<Tag> tags;
    private ComponentMapper<PhysicsBody> bodies;

    @Override
    protected void createArchetypes() {
        //noinspection unchecked
        playerArchetype = new ArchetypeBuilder()
                .add(
                        ActorComponent.class,
                        PhysicsBody.class,
                        Tag.class
                )
                .build(world);
    }

    public Archetype getPlayerArchetype() {
        return playerArchetype;
    }

    public int createPlayer() {
        return world.create(playerArchetype);
    }
}
