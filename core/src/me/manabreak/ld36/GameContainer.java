package me.manabreak.ld36;

import com.artemis.World;
import com.artemis.WorldConfiguration;
import com.artemis.WorldConfigurationBuilder;
import com.badlogic.gdx.utils.viewport.ExtendViewport;

import me.manabreak.ld36.factories.NodeManager;
import me.manabreak.ld36.factories.PlayerFactory;
import me.manabreak.ld36.systems.DirectorSystem;
import me.manabreak.ld36.systems.ElevatorSystem;
import me.manabreak.ld36.systems.GuiSystem;
import me.manabreak.ld36.systems.MapSystem;
import me.manabreak.ld36.systems.ParticleSystem;
import me.manabreak.ld36.systems.PhysicsSystem;
import me.manabreak.ld36.systems.StageSystem;

public class GameContainer {

    private final World world;
    private final Player player;

    public GameContainer() {

        WorldConfiguration config = new WorldConfigurationBuilder()
                .with(
                        new PhysicsSystem(),
                        new MapSystem(),
                        new ParticleSystem(),
                        new ElevatorSystem(),
                        new StageSystem(new ExtendViewport(90f, 90f)),
                        new GuiSystem(),
                        new DirectorSystem()
                )
                .withPassive(WorldConfigurationBuilder.Priority.LOWEST,
                        new PlayerFactory(),
                        new NodeManager()
                )
                .build();

        world = new World(config);
        player = new Player(world);
        world.getSystem(DirectorSystem.class).setPlayer(player);

        world.inject(player);
        player.initialize();

        MapSystem mapSystem = world.getSystem(MapSystem.class);
        mapSystem.addListener(world.getSystem(NodeManager.class));
        mapSystem.addListener(player);
        mapSystem.addListener(world.getSystem(PhysicsSystem.class));
        mapSystem.addListener(world.getSystem(DirectorSystem.class));

        mapSystem.loadMap(0);
    }

    public void update(float dt) {
        player.act(dt);
        world.setDelta(dt);
        world.process();
    }
}
